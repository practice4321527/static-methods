﻿namespace StaticMethods
{
    public static class PassthroughMethods
    {
        // TODO #2-1: Add the static method here with name "ReturnInt" that gets "intParameter" parameter ("int" type) and returns it.

        // TODO #2-2: Add the static method here with name "ReturnUnsignedInt" that gets "uintParameter" parameter ("uint" type) and returns it.

        // TODO #2-3: Add the static method here with name "ReturnLong" that gets "longParameter" parameter ("long" type) and returns it.

        // TODO #2-4: Add the static method here with name "ReturnUnsignedLong" that gets "ulongParameter" parameter ("ulong" type) and returns it.

        // TODO #2-5: Add the static method here with name "ReturnFloat" that gets "floatParameter" parameter ("float" type) and returns it.

        // TODO #2-6: Add the static method here with name "ReturnDouble" that gets "doubleParameter" parameter ("double" type) and returns it.

        // TODO #2-7: Add the static method here with name "ReturnDecimal" that gets "decimalParameter" parameter ("decimal" type) and returns it.

        // TODO #2-8: Add the static method here with name "ReturnString" that gets "stringParameter" parameter ("string" type) and returns it.

        // TODO #2-9: Add the static method here with name "ReturnChar" that gets "charParameter" parameter ("char" type) and returns it.

        // TODO #2-10: Add the static method here with name "ReturnByte" that gets "byteParameter" parameter ("byte" type) and returns it.

        // TODO #2-11: Add the static method here with name "ReturnBool" that gets "boolParameter" parameter ("bool" type) and returns it.
    }
}
